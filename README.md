# README #

### What is this repository for? ###

* Wide Field of View (WFoV) camera calibration example using OpenCV. We estimated camera intrinsic parameters of the LG G5 smartphone camera, which has a WFoV sensor (135 degrees).
* We have used the FishEye model that is implemented in OpenCV.
* Reprojection error after computing camera intrinsic parameters is about 0.5 pixels, which is OK for FullHD images (1920x1080)

### How do I get set up? ###

* CMake
* OpenCV 3.3.0
* Windows 10
* Print OpenCV [chessboard](http://docs.opencv.org/2.4/_downloads/pattern.png)
* Check directory parameterson the XML config file (in_VID.xml && VID.xml)
* We provide a set of images as an example for testing the code (OpenCV example).
* Setup the project using the provided CMake configuration file

### Results ###

* This example computes intrinsic parameters (K matrix) of the camera and exports those parameters to a config file: out_camera_data.xml

Input image -> Undistorted image: 

![Results](markdown/undistored_image.png "Undistorted")

### Get in touch ###

* sorts@ua.es Sergio Orts-Escolano